window.addEventListener('navigate', event => {
  document.body.className = event.detail.route;
  //activates scrollify only on the home view
  if(event.detail.route == 'home') 
    {
      $.scrollify.enable();
    }
  else {$.scrollify.disable();} ;
  $('header').removeClass('home-clients').removeClass('home-talents');
  window.scrollTo(0, 0);
});

//analytics
var _paq = window._paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
  var u="//stats.happy-dev.fr/";
  _paq.push(['setTrackerUrl', u+'matomo.php']);
  _paq.push(['setSiteId', '1']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
})();
{
  let url = location.href;
  document.addEventListener('navigate', () => {
     if(url===location.href) return;
     url = location.href;
    _paq.push(['setCustomUrl', url]);
    _paq.push(['trackPageView']);
  });
}

jQuery(function($){
  document.addEventListener('scroll', event => {
    //parallax setup
    const scrollPos = $(document).scrollTop();
    $('.parall-1').css({
      'transform': `translate(0px, -${scrollPos / 9}px)`
    });
    $('.parall-2').css({
      'transform': `translate(0px, -${scrollPos / 5}px)`
    });
    $('.parall-3').css({
      'transform': `translate(${scrollPos /50}px, ${scrollPos /15}px)`
    });

    //scroll FadeIn
    $('.appear').each( function(i){
      var textPos = $(this).offset().top + 170 ;
      var bottomWindow = $(window).scrollTop() + $(window).height();
      if(bottomWindow > textPos) 
      { 
        $(this).addClass("show")
       }
      else { 
        $(this).removeClass("show")
      }
    });
  });  
  
  //closes the menu when an item is open
  $('solid-route').click(function() {
    $('details').removeAttr('open');
  });
  
  $('footer .footer-mobile solid-link').click(function() {
    $('footer details').removeAttr('open');
  });
  
  
  $.scrollify({
    section : "#home section",
    sectionName: "section-name",
    standardScrollElements: ".clients-logo",
    updateHash: false,
    before:function(i,panels) {
      var ref = panels[i].attr("data-section-name");
      $(".pagination .active").removeClass("active");
      $(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
    },    
    after:function(i,panels) {
      var ref = panels[i].attr("data-section-name");
      $("header").removeClass().addClass(ref);
      $(".pagination").removeClass().addClass("pagination").addClass(ref);
      
    }
  });
  $(".pagination a,a.pagicon").click(function(e) {
    e.preventDefault();
		$.scrollify.move($(this).attr("href"));
  });
  
  // contact form
  $("#contact solid-form").on("populate", function (e) {
    const fields = {
      "name": "howareyou",
      "howareyou": "howhelp",
      "domaine":"clientlocation",
      "clientlocation":"workwish",
      "workwish":"project",
      "project":"document",
      "document":"budget",
      "budget":"email",
      "textfreelance":"email",
      "textporteur":"email",
      "textautre":"email",
      "email":"phone"
    };
    
    const paths = [0, "domaine", "textfreelance", "textporteur", "textautre" ];
    function getLastField() {
      const visibleFields = document.querySelectorAll("#contact solid-form > :not(.hide-field)");
      return visibleFields[visibleFields.length-1];
    }
    function showNextField(fieldName) {
      if (fieldName=="howhelp") {
        var nextField = paths[document.querySelector("select[name=howhelp]").selectedIndex];
        $('.need-help').addClass("hide-field").hide();
      }
      else  {
        var nextField = fields[fieldName];
      };
      $( ".contact label" ).parents().removeClass("conversation");
      $( ".contact label" ).parents('[name=' + nextField + ']').fadeIn("slow").removeClass("hide-field");
      getLastField().classList.add("conversation");
      if (fieldName=="name") {
        var nameText = $('.name-field input').val();
        $('.howareyou-field div ').text("Enchantée " + nameText +", comment vas-tu ?");
      };
      if (nextField=="howareyou") {
        $( "progress" ).val(20);
      }
      else if (nextField=="howhelp") {
        $( "progress" ).val(30);
      }
      else if (nextField=="domaine") {
        $( "progress" ).val(40);
      }
      else if (nextField=="clientlocation") {
        $( "progress" ).val(50);
      }
      else if (nextField=="workwish") {
        $( "progress" ).val(60);
      }
      else if (nextField=="document") {
        $( "progress" ).val(70);
      }
      else if (nextField=="budget" || nextField=="textfreelance" || nextField=="textporteur" || nextField=="textautre") {
        $( "progress" ).val(80);
      }
      else if (nextField=="email") {
        $( "progress" ).val(90);
      }
      else if (nextField=="phone") {
        $( "progress" ).val(100);
      }
    };
    function submitForm() {
      var errorCount = 0;
      $(".err-msg").remove();
      if ($("#contact .name-field input").val()=="") {
        $('<div class="err-msg">Le nom est obligatoire</div>').insertBefore($("#contact .green-btn"));
        errorCount = errorCount + 1;
      };
      if ($("#contact [name=email] input").val()=="") {
        $('<div class="err-msg">Un e-mail est obligatoire</div>').insertBefore($("#contact .green-btn"));
        errorCount = errorCount + 1;
      };
      if (errorCount == 0) {
        console.log ("go");
        document.querySelector('#contact solid-form').component.submitForm();
      };
    };

    $('#contact solid-form').change(function(event) {
      showNextField(event.target.getAttribute("name")) 
    })

    $('#contact button').click(function() {
      const currentFieldName = getLastField().getAttribute("name");
      if (currentFieldName=="phone") {
        submitForm();
      }
      else {
        showNextField(currentFieldName);
      }
    });
  
    $('input').keyup( event => {
      if (event.key == "Enter") {
          if ($('.phone input').val()) {
            submitForm();
          }
        }
    });


      $('#contact solid-form').on('save', event => { 
        $('#contact solid-form').fadeOut("slow");
        $('#contact button').fadeOut("slow");
        $('#contact .progressing-bar-container').fadeOut("slow");
        $('.confirmation-message').fadeIn("slow");
      }) 
  });

  //random numbers carousel
  $('.btn-number, .manette').click(function() {
    var numb = $('#carousel1 .carousel-inner .carousel-item').length;
    var randomSlide = Math.floor(Math.random() * numb);
    $('#carousel1').carousel(randomSlide);
    var numb2 = $('#carousel2 .carousel-inner .carousel-item').length;
    var randomSlide2 = Math.floor(Math.random() * numb2);
    $('#carousel2').carousel(randomSlide2);
    var numb3 = $('#carousel3 .carousel-inner .carousel-item').length;
    var randomSlide3 = Math.floor(Math.random() * numb3);
    $('#carousel3').carousel(randomSlide3);
  });

  //collectives appear
  $("#collectives solid-display").on("populate", function (e) {
    setTimeout(() => {
      $("#collectives solid-display").addClass("appear");
    })
  });
  $("#evenements solid-display").on("populate", function (e) {
    setTimeout(() => {
      $('#evenements .events-grid solid-display').addClass("appear");
    })
  });

  //popup event
  $("#evenements dialog solid-display").on("populate", function (e) {
    var valUrl = window.location.href;
    $('.fbshare').attr("data-href", valUrl);
    $('.fbshare a').attr("href", "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(valUrl) + "&amp;src=sdkpreparse");
    $('.twshare').attr("href", "https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fhappy-dev.fr%2Fevenements&amp;ref_src=twsrc%5Etfw&amp;tw_p=tweetbutton&amp;url=" + valUrl + "&amp;");
    $('.lkshare').attr("href","https://www.linkedin.com/shareArticle?mini=true&url=" + encodeURIComponent(valUrl));
  });
  document.addEventListener('click', event => {
    if(!event.target.tagName  === "DIALOG") return;
    const close = event.target.querySelector('.mdi-window-close');
    if(!close) return;
    close.click();
  })

  //faq display
  $("#faq solid-display").on("populate", function (e) {
    setTimeout(() => {
      $("#faq solid-display solid-group-default > span").click(function() {
        $(this).parent().toggleClass('closed');
      });
    });
  });
  
  $('.FAQ-section solid-display').click(function() {
    window.dispatchEvent(new CustomEvent('requestNavigation',  {detail:  {route: "faq"}}));
    location.hash = this.component.resource["@id"];
    location.hash = this.component.resource["@id"];
  });

  $('.languagechoice').click(function() {
    var pathAfterThePrefix = window.location.pathname.split('/')[2];
    var base_url = window.location.host
    var lang = window.location.pathname.split('/')[1];
    if ( lang == "fr") {
      //Redirection with the appropriate prefixe.
      var redirect = 'http://'+base_url+'/en/'+pathAfterThePrefix

      window.location.href = redirect
    } else {
      var redirect = 'http://'+base_url+'/fr/'+pathAfterThePrefix
      window.location.href = redirect
    }
  })

});

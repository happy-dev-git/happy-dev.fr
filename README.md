# HAPPY-DEV.FR
Ceci est un portage sauvage du site [happy-dev.fr](https://happy-dev.fr) en format statique.

## ToDo
- Revoir les liens et menu (ex: lien vers les pages du wiki)
- Ajouter la page des mentions légales et autre si besoin
- Nettoyer le CSS et le code (beaucoup de code inutile)
- Sélectionner quelques clients et projets a la place de l'infinie liste de clients
- Réduire radicalement le nombre de polices
- Ajouter des trucs à faire ici 😁

## Le code
Pour migrer du code du site d'origine, il est nécessaire de faire un peu de nettoyage. Le site d'origine est fait avec Solid, cela vient donc avec un balisage html non standard.

Pour conserver les formatages, j'ai converti toutes les règles CSS vers ses balises en règles vers une classe du même nom. Par exemple :
```css
solid-link {
    color: #000;
}
```
Devient :
```css
.solid-link {
    color: #000;
}
```

Ce qui permet de repasser sur un balisage html standard :
```html
<solid-link next="toto">Toto</solid-link>
```
Devient :
```html
<a href="toto" class="solid-link">Toto</a>
```

Si vous avez besoin d'une regex pour ce cas précis, voici la mienne :
```regex
<solid-link (?:class="([^"]+)" )?(?:next="([^"]+)")(?: class="([^"]+)")? ?> *(.+(?!<solid)) *<\/solid-link>
```
Et le remplacement :
```regex
<a href="$2" class="solid-link $1$3">$4</a>
```
Attention cependant, des liens se trouvent dans des boutons, et des liens contiennent des balises block... Il faudra donc faire un peu de nettoyage manuel dans tous les cas.

## Organisation
```
/ public: contient le site web statique
|
| - /media
|   |
|   | - /img : les images du site
|   |   | - *.(svg|png|jpg...)
|   |   | - ...
|   |
|   | - /clients : les logos des clients
|   |   | - *.webp
|   |   | - ...
|   |
|   | - /fonts : les polices du site
|       | - *.(ttf|woff2)
|       | - ...
|
| - /styles : les fichiers CSS
|   | - *.css
|   | - ...
|
| - index.html : la page d'accueil
| - *.html : les autres pages
| - ...
```

## Tester en local

Pour avoir une version de dev, il suffit de lancer un serveur web dans le dossier /public. Il y en a avec beaucoup de langages de programmation, par exemple avec PHP:
```bash
php -S localhost:8000 -t public
```

Vous pouvez aussi utiliser docker:
```bash
docker compose up -d
```
Le site est ensuite accessible via : [http://localhost:3000](http://localhost:3000)

## Mise en ligne
Rien de complexe, c'est juste du static dans le dossier /public. 

Pour la mise en prod, c'est un serveur web classique fournis par gitlab pages. En pushant sur la branche master, le site est mis en ligne automatiquement.

## C'est que quoi ?
J'avais débuté un nouveau code au propre, mais c'etait idiot et clairement pas le moment. Je n'ai cepandant pas supprimé ce brouillon, il est possible dans le dossier /brouillon.
- brouillon/style.css
- brouillon/index.html